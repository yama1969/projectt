﻿using UnityEngine;
using System.Collections;

public class BtnAction : MonoBehaviour {

	// 属性弾フラグ　0:通常　1:火　2:氷　3:雷
	public static int bulletFlg = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	/// タイトル：STARTボタン押下
	public void ClickStart ()
	{
		// メニュー画面へ遷移
		Application.LoadLevel("Menu");		
	}
	
	/// メニュー：STAGE１ボタン押下
	public void ClickStage1 ()
	{
		// メイン画面へ遷移
		Application.LoadLevel("Main");		
	}
	
	/// メイン：Weapon１ボタン押下
	public void ClickWeapon1 ()
	{
		// 属性弾フラグセット
		if(bulletFlg == 1){
			bulletFlg = 0;
		}else{
			bulletFlg = 1;
		}
		Debug.Log("1:" + bulletFlg);
	}
	/// メイン：Weapon２ボタン押下
	public void ClickWeapon2 ()
	{
		// 属性弾フラグセット
		if(bulletFlg == 2){
			bulletFlg = 0;
		}else{
			bulletFlg = 2;
		}
		Debug.Log("2:" + bulletFlg);
	}
		/// メイン：Weapon３ボタン押下
	public void ClickWeapon3 ()
	{
		// 属性弾フラグセット
		if(bulletFlg == 3){
			bulletFlg = 0;
		}else{
			bulletFlg = 3;
		}
		Debug.Log("3:" + bulletFlg);
	}
}
