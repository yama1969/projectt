﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : Token {

	// 制限時間テキスト
	public Text time;
	// スコアテキスト
	public Text score;
	// 討伐数テキスト
	public Text enemyCnt;
	// クリア画像
	public Image clearImg;
	// ゲームオーバー画像
	public Image gameoverImg;
	// HP
	public static Image lifeImg;
	// 経過時間
	float lapseTime = 0.0f;
	
	// 位置座標
	private Vector3 position;
	// スクリーン座標をワールド座標に変換した位置座標
	private Vector3 screenToWorldPointPosition;
	
	// 弾の発射感覚
	private float shotTime = 0.3f;
	// 弾発射フラグ
	private bool shotFlg = true;
	// 討伐ノルマ
	int enemyCount = 0;
	// プレイヤーHP
	static float playerLife = 0;
	// プレイヤー最大HP
	static float playerMaxLife = 0;
		
	void Start () {
		// 初期化
		Util.ClearCount();
		playerLife = 5.0f;
		playerMaxLife = playerLife;
		enemyCount = 15;
		Enemy.count = enemyCount;
		MakeEnemys(5);
		time = GameObject.Find("Canvas/Panel/Time").GetComponent<Text>();
		score = GameObject.Find("Canvas/Panel/Score").GetComponent<Text>();
		enemyCnt = GameObject.Find("Canvas/Panel/Enemy").GetComponent<Text>();
		clearImg = GameObject.Find("Canvas/Panel/Clear").GetComponent<Image>();
		gameoverImg = GameObject.Find("Canvas/Panel/GameOver").GetComponent<Image>();
		lifeImg = GameObject.Find("Canvas/Panel/Life").GetComponent<Image>();
		clearImg.enabled = false;
		gameoverImg.enabled = false;
	}
	
	void Update () {
		// 時間カウント
	 	CountTime();
		// スコア表示
		score.text = Util.GetScore().ToString();
		// 討伐数表示
		enemyCnt.text = GetEnemyCnt();
		// タッチ検出
        if (Input.GetMouseButtonDown(0))
        {
            if (shotFlg)
            {
				// 使用弾カウント
				if(clearImg.enabled == false){
					Util.AddUseBullet(1);
					Debug.Log(Util.GetUseBullet());
				}
                // フラグOFF
                shotFlg = false;
                // Vector3でマウス位置座標を取得する
                position = Input.mousePosition;
                // Z軸修正
                position.z = 0.0f;
                // マウス位置座標をスクリーン座標からワールド座標に変換する
                screenToWorldPointPosition = Camera.main.ScreenToWorldPoint(position);
                // ワールド座標に変換されたマウス座標を代入
                gameObject.transform.position = screenToWorldPointPosition;
                // タッチした場所にエフェクトを表示
                Bullet_s.Add(screenToWorldPointPosition.x, -5);
                // 数秒後結果画面へ遷移
                StartCoroutine(ShootBullet(shotTime));
            }
        }
		
		// 討伐数0ならクリア
        if (Enemy.count <= 0)
        {
			// 討伐数がマイナスにならないようにする
			Enemy.count = 0;
			// クリア画像表示
			clearImg.enabled = true;
			// 数秒後結果画面へ遷移
			StartCoroutine(MoveResult(2.0F));
        }
		
		if(GameManager.playerLife <= 0)
		{
			// HPがマイナスにならないようにする
			GameManager.playerLife = 0;
			// ゲームオーバ画像表示
			gameoverImg.enabled = true;
			// 数秒後結果画面へ遷移
			StartCoroutine(MoveResult(2.0F));			
		}
    }
	
	// 敵生成
	public void MakeEnemys(int cnt){
		for(int i = 0; i < cnt; i++){
			Vector3 p = new Vector3(
				Random.Range(-7,7),	4, 0);
			Enemy.Add(p.x, p.y);
		}
	}

    // 時間カウント
    void CountTime()
    {
        float deltaTime = Time.deltaTime;
        lapseTime += deltaTime;
     	time.text = ("" + lapseTime.ToString("F2"));
	}
	
	// 討伐数
	string GetEnemyCnt(){ 
		return Enemy.count.ToString() + "／" + enemyCount.ToString();
	}
	    
	// 弾発射ディレイ
    IEnumerator ShootBullet(float time)
    {
		yield return new WaitForSeconds(time);
		shotFlg = true;
   }

	// ディレイ
    IEnumerator MoveResult(float time)
    {
		yield return new WaitForSeconds(time);
 		Application.LoadLevel("Result");
    }
   
    // ライフゲージ減少
    public static void PlayerDamage(int damage)
	{
		playerLife -= damage;
		float scaleBar = 1.0f - (1.0f - (playerLife / playerMaxLife));
		float hp = playerLife / playerMaxLife;
		Debug.Log(playerLife + " " + playerMaxLife + " " + hp);
		Debug.Log("scaleBar " + scaleBar + " playerLife " + playerLife + " " + lifeImg.transform.localScale.x);
		
		lifeImg.transform.localScale = new Vector3(scaleBar,1,1);
	}
}
