﻿using UnityEngine;
using System.Collections;

public class Util : Token {

	// スコア
	public static int score;
	// 場外弾
	public static float missBullet;
	// 使用弾数
	public static float useBullet;
	
    /// スコア初期化
    public static void ClearCount(){
		score = 0;
		missBullet = 0;
		useBullet = 0;
    }
    /// スコアカウント
    public static void AddScore(int cnt){
		score += cnt;
    }

	// スコア取得	
	public static int GetScore(){
		return score;
	}
	
	/// 場外弾カウント
    public static void AddMissBullet(int cnt){
		missBullet += cnt;
    }

	// 場外弾取得	
	public static float GetMissBullet(){
		return missBullet;
	}
	
	/// 使用弾数カウント
    public static void AddUseBullet(int cnt){
		useBullet += cnt;
    }

	// 使用弾数取得	
	public static float GetUseBullet(){
		return useBullet;
	}
}
