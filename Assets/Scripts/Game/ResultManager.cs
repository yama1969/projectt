﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResultManager : MonoBehaviour {
	
	// スコア
	public Text score;
	// 命中率
	public Text hitScore;
	
	// タップ許可フラグ
	bool tapFlg = false;
	
	// Use this for initialization
	void Start () {
		// 命中率
		float hitPer = 0.0f;
		// コンポーネント取得
		score = GameObject.Find("Canvas/Panel/Score").GetComponent<Text>();
		hitScore = GameObject.Find("Canvas/Panel/HitScore").GetComponent<Text>();
        // スコア設定
        score.text = ("" + Util.GetScore());
        if (Util.GetUseBullet() > 0)
        {
			Debug.Log("MISS " + Util.GetMissBullet());
			Debug.Log("USE " + Util.GetUseBullet());
			
            hitPer = ((Util.GetUseBullet() - Util.GetMissBullet()) / Util.GetUseBullet()) * 100;
        }
        hitScore.text = ("" + hitPer.ToString("f2") + "%");
		
		// 数秒後結果画面へ遷移
        StartCoroutine(moveScene(2.0f));
	}
	
	// Update is called once per frame
	void Update () {
			// タッチ検出
        if (Input.GetMouseButtonDown(0) && tapFlg == true)
        {
			 Application.LoadLevel("Menu");
        }
	}
	
	// 画面タップ許可ディレイ
    IEnumerator moveScene(float time)
    {
		yield return new WaitForSeconds(time);
		tapFlg = true;
   }
}
