﻿using UnityEngine;
using System.Collections;

public class Bullet_s : Token
{
    /// プレハブ
    static GameObject _prefab = null;
    static GameObject _prefab0 = null;
    static GameObject _prefab1 = null;
    static GameObject _prefab2 = null;
    static GameObject _prefab3 = null;
    /// オブジェクト生成
    static GameObject go = null;
    static GameManager gm = null;
        
    /// 開始
    void Start()
    {
        // サイズを設定
        SetSize(SpriteWidth / 2, SpriteHeight / 2);
    
        // 下方向に移動する
        SetVelocityXY(0, 6);
        
        go = GameObject.Find("GameManager");
        if(go != null){
            gm = go.GetComponent(typeof(GameManager)) as GameManager;
        }
    }
    
    void Update (){
        // 画面外にでたら弾が消える
        if(IsOutside())
        {
            // 場外弾アップ
            if(Enemy.count >= 0){
               Util.AddMissBullet(1);
            }
            DestroyObj();   
        }
   }
    
    /// プレハブの生成
    public static Bullet_s Add(float x, float y)
    {

        // プレハブを取得
        if (BtnAction.bulletFlg == 1)
        {
            _prefab1 = GetPrefab(_prefab1, "Effects/fire_bullet");
    		_prefab = _prefab1;
        }
        else if (BtnAction.bulletFlg == 2)
        {
            _prefab2 = GetPrefab(_prefab2, "Effects/ice_bullet");
    		_prefab = _prefab2;
        }
        else if (BtnAction.bulletFlg == 3)
        {
            _prefab3 = GetPrefab(_prefab3, "Effects/thunder_bullet");
    		_prefab = _prefab3;
        }
        else
        {
            _prefab0 = GetPrefab(_prefab0, "Effects/bullet1");
    		_prefab = _prefab0;
        }
        
        // プレハブからインスタンスを生成
        return CreateInstance2<Bullet_s>(_prefab, x, y);
    }
    
    // アニメーション消滅
	void OnAnimationFinish(){
		Destroy(gameObject);
	}
}
