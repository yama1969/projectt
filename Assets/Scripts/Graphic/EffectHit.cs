﻿using UnityEngine;
using System.Collections;

public class EffectHit : Token
{
    /// プレハブ
    static GameObject _prefab = null;
    /// プレハブの生成
    public static EffectHit Add(float x, float y)
    {
        // プレハブを取得
        _prefab = GetPrefab(_prefab, "Effects/hitEffect");
        // プレハブからインスタンスを生成
        return CreateInstance2<EffectHit>(_prefab, x, y);
    }

    // アニメーション消滅
    void OnAnimationFinish()
    {
        Destroy(gameObject);
    }
}
