﻿using UnityEngine;
using System.Collections;

/// 敵
public class Enemy : Token
{
    /// プレハブ
    static GameObject _prefab = null;

    /// オブジェクト生成
    static GameObject go = null;
    static GameManager gm = null;
    /// 敵生存数
    public static int count = 15;
	
    // 画像切替
    SpriteRenderer MainSpriteRenderer;
    public Sprite MainSprite;
    public Sprite AttackSprite;

    /// 開始
    void Start()
    {
        // サイズを設定
        SetSize(SpriteWidth / 2, SpriteHeight / 2);

        // 下方向に移動する
        SetVelocityXY(0, -1);
        //
        go = GameObject.Find("GameManager");
        if (go != null)
        {
            gm = go.GetComponent(typeof(GameManager)) as GameManager;
        }
        // 画像切り替え用コンポーネントを取得
        MainSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    /// 更新
    void Update()
    {
        // カメラの左下座標を取得
        Vector2 min = GetWorldMin();
        // カメラの右上座標を取得する
        Vector2 max = GetWorldMax();

        if (X < min.x || max.x < X)
        {
            // 画面外に出たので、X移動量を反転する
            VX *= -1;
            // 画面内に移動する
            ClampScreen();
        }
        if (Y < min.y || max.y < Y)
        {
            // 画面外に出たので、Y移動量を反転する
            VY *= -1;
            // 画面内に移動する
            ClampScreen();
        }
    }

    // ぶつかった瞬間に呼び出される
    void OnTriggerEnter2D(Collider2D c)
    {
        // 当たった対象のレイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);
        // DeadLineに激突
        if (layerName == "DeadLine")
        {
            // 攻撃エフェクト
            MainSpriteRenderer.sprite = AttackSprite;
            EffectHit.Add(X, Y);

            // HP減少
            GameManager.PlayerDamage(1);
            // HP0の場合、ゲームオーバ

            // 数秒後オブジェクトを消す
            StartCoroutine(Attack(0.2f));
        }
        else if (layerName == "Bullet")
        {
            // 弾の削除
            Destroy(c.gameObject);
            // 破棄する
            DestroyObj();
            // 生存数を減らす
            count--;
            // スコアアップ
            if(count >= 0){
                Util.AddScore(100);
            }
            // 敵のHP減少
            
            // 敵HP0の場合、消滅
            
            // 死にエフェクト
            EffectDead.Add(X, Y);
        }
        // 殲滅数が0でなければ敵追加
        if (count > 0)
        {
            gm.MakeEnemys(1);
        }
    }

    // ディレイ
    IEnumerator Attack(float time)
    {
        yield return new WaitForSeconds(time);

        // 破棄する
        DestroyObj();
    }

    /// プレハブの生成
    public static Enemy Add(float x, float y)
    {
        // プレハブを取得
        _prefab = GetPrefab(_prefab, "Charactors/enemy");
        // プレハブからインスタンスを生成
        return CreateInstance2<Enemy>(_prefab, x, y);
    }
}