﻿using UnityEngine;
using System.Collections;

/// 死にエフェクト
public class EffectDead : Token
{
    /// プレハブ
    static GameObject _prefab = null;
    /// プレハブの生成
    public static EffectDead Add(float x, float y)
    {
        // プレハブを取得
        _prefab = GetPrefab(_prefab, "Effects/deadEffect");
        // プレハブからインスタンスを生成
        return CreateInstance2<EffectDead>(_prefab, x, y);
    }
    
    // アニメーション消滅
	void OnAnimationFinish(){
		Destroy(gameObject);
	}
}
